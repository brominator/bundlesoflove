---
layout: page
title: Contact
permalink: /contact/
---
Abigail Embree  
Email: bundlesoflove@embr.ee  
Phone: (814) 374-3615  
Facebook: [https://www.facebook.com/Bundles-of-Love-Baby-Clothing-Library-175016166500277](https://www.facebook.com/Bundles-of-Love-Baby-Clothing-Library-175016166500277)
