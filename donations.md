---
layout: page
title: Donations
permalink: /donations/
---
If you would like to donate to Bundles of Love please send me an email at bundlesoflove@embr.ee or a Facebook message [here](https://www.facebook.com/Bundles-of-Love-Baby-Clothing-Library-175016166500277) and I will be happy to schedule a meeting. I'm often available to pick up within a reasonable distance.
